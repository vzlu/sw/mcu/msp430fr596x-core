#ifndef _MSP430FR596X_UART_H
#define _MSP430FR596X_UART_H

#include "core.h"
#include "simple_buff.h"

typedef void (*uart_cb_t)(void);

typedef enum {
  BLOCKING = 0,
  INTERRUPT,
  DMA
} uart_xfer_mode;

// UART configuration structure
struct uart_cfg_t {
  // pointer to UART peripheral base
  const TINY_WORD_TYPE* base;
  
  // buad rate
  uint32_t baudRate;
  
  // Tx/Rx callbacks
  uart_cb_t txCb, rxCb;
  
  // pointers to Rx/Tx buffers - allocated by user
  uint8_t* rxBuffPtr;
  uint8_t* txBuffPtr;
  
  // Rx/Tx buffer sizes
  uint16_t rxBuffSize, txBuffSize;
  
  // transfer mode
  uart_xfer_mode txMode;
  
  // flag to prevent re-initialization
  uint8_t initialized;
  
  // Rx/Tx ring buffers
  volatile simple_buffer_t txRing, rxRing;
};

// ugly function-like macro to create IRQ handler
#define UART_IRQ_HANDLER(VECTOR_NAME, HANDLER, UART_CFG)                        \
void __attribute__ ((interrupt(VECTOR_NAME))) HANDLER (void) {                  \
  switch(__even_in_range(TINY_REGW(UART_CFG.base + UCxnIV), 8)) {               \
    case USCI_NONE:                                                             \
      break;                                                                    \
    case USCI_UART_UCRXIFG: {                                                   \
      volatile uint8_t ch = (uint8_t)TINY_REGW(UART_CFG.base + UCxnRXBUF);      \
      simple_buffer_add_from_isr(&(UART_CFG.rxRing), ch);                       \
      if(UART_CFG.rxCb) {                                                       \
        scheduler_add_from_isr(UART_CFG.rxCb, true);                            \
      }                                                                         \
    } break;                                                                    \
    case USCI_UART_UCTXIFG: {                                                   \
    } break;                                                                    \
    case USCI_UART_UCSTTIFG: {                                                  \
    } break;                                                                    \
    case USCI_UART_UCTXCPTIFG: {                                                \
      uint8_t ch;                                                               \
      if(!simple_buffer_take_from_isr(&(UART_CFG.txRing), &ch)) {               \
        TINY_REGW(UART_CFG.base + UCxnIE) &= ~UCTXCPTIE;                        \
        break;                                                                  \
      }                                                                         \
      TINY_REGW(UART_CFG.base + UCxnTXBUF) = ch;                                \
      if(UART_CFG.txCb) {                                                       \
        scheduler_add_from_isr(UART_CFG.txCb, true);                            \
      }                                                                         \
    } break;                                                                    \
    default:                                                                    \
      break;                                                                    \
  }                                                                             \
}

void uart_init(struct uart_cfg_t* uart);
void uart_deinit(struct uart_cfg_t* uart);
void uart_putchar(struct uart_cfg_t* uart, char ch);
char uart_getchar(struct uart_cfg_t* uart, char *ch);
void uart_write_blocking(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len);
void uart_write_interrupt(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len);
void uart_write_dma(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len);
void uart_finish_dma(struct uart_cfg_t* uart);
void uart_write(struct uart_cfg_t* uart, uint8_t *buff, uint16_t len);
void uart_poll_input(struct uart_cfg_t* uart, uart_cb_t cb);
void uart_dma_xfer(struct uart_cfg_t* uart);

#endif // _MSP430FR596X_UART_H
