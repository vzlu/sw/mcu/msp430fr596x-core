#ifndef _MSP430FR596X_I2C_H
#define _MSP430FR596X_I2C_H

#include "core.h"
#include "simple_buff.h"

typedef void (*i2c_slave_cb_t)(volatile simple_buffer_t* buff, uint16_t len);

// I2C configuration structure
struct i2c_cfg_t {
  // pointer to I2C peripheral base
  const TINY_WORD_TYPE* base;
  
  // clock frequency
  uint32_t freq;
  
  // slave address
  uint8_t addr;
  
  // Rx callback
  i2c_slave_cb_t rxCb;
  
  // pointers to Rx/Tx buffers - allocated by user
  uint8_t* rxBuffPtr;
  uint8_t* txBuffPtr;
  
  // Rx/Tx buffer sizes
  uint16_t rxBuffSize, txBuffSize;
  
  // flag to prevent re-initialization
  uint8_t initialized;
  
  // Rx/Tx ring buffers
  volatile simple_buffer_t txRing, rxRing;
  
  volatile uint8_t busy;
  volatile uint8_t rx;
};

#define I2C_IRQ_HANDLER(VECTOR_NAME, HANDLER, I2C_CFG)                          \
void __attribute__ ((interrupt(VECTOR_NAME))) HANDLER (void) {                  \
  i2c_set_irq_inst(&I2C_CFG);                                                   \
  switch(__even_in_range(TINY_REGW(I2C_CFG.base + UCBnIV), USCI_I2C_UCBIT9IFG)) { \
    case USCI_NONE:                                                             \
      break;                                                                    \
    case USCI_I2C_UCALIFG:                                                      \
      /* Vector 2: ALIFG, lost arbitration */                                   \
      TINY_REGW(I2C_CFG.base + UCBnIFG) &= ~(UCALIFG);                          \
      scheduler_add_from_isr(i2c_isr_reinit, false);                            \
      break;                                                                    \
    case USCI_I2C_UCNACKIFG:                                                    \
      /* Vector 4: NACKIFG, NACK, then I was a master */                        \
      TINY_REGW(I2C_CFG.base + UCxnCTLW0) |= UCTXSTP;                           \
      scheduler_add_from_isr(i2c_isr_tx_done, true);                            \
      break;                                                                    \
    case USCI_I2C_UCSTTIFG:                                                     \
      /* Vector 6: STTIFG, START Condition */                                   \
      TINY_REGW(I2C_CFG.base + UCBnIFG) &= ~(UCSTPIFG);                         \
      I2C_CFG.busy = 1;                                                         \
      break;                                                                    \
    case USCI_I2C_UCSTPIFG:                                                     \
      /* Vector 8: STPIFG, STOP condition - Tx or Rx successful */              \
      TINY_REGW(I2C_CFG.base + UCBnIFG) &= ~(UCTXIFG0);                         \
      if(I2C_CFG.rx) {                                                          \
        scheduler_add_from_isr(i2c_isr_rx_done, true);                          \
      }                                                                         \
      break;                                                                    \
    case USCI_I2C_UCRXIFG3:                                                     \
      break;                                                                    \
    case USCI_I2C_UCTXIFG3:                                                     \
      break;                                                                    \
    case USCI_I2C_UCRXIFG2:                                                     \
      break;                                                                    \
    case USCI_I2C_UCTXIFG2:                                                     \
      break;                                                                    \
    case USCI_I2C_UCRXIFG1:                                                     \
      break;                                                                    \
    case USCI_I2C_UCTXIFG1:                                                     \
      break;                                                                    \
    case USCI_I2C_UCRXIFG0: {                                                   \
      /* Vector 22: RXIFG0, RX bytes */                                         \
      volatile uint8_t rx = TINY_REGW(I2C_CFG.base + UCxnRXBUF);                \
      simple_buffer_add_from_isr(&(I2C_CFG.rxRing), rx);                        \
    } break;                                                                    \
    case USCI_I2C_UCTXIFG0: {                                                   \
      /* Vector 24: TXIFG0 */                                                   \
      if(simple_buffer_is_empty_from_isr(&(I2C_CFG.txRing))) {                  \
        TINY_REGW(I2C_CFG.base + UCxnCTLW0) |=  UCTXSTP;                        \
        TINY_REGW(I2C_CFG.base + UCBnIFG)   &= ~UCTXIFG;                        \
        scheduler_add_from_isr(i2c_isr_tx_done, true);                          \
      } else {                                                                  \
        volatile uint8_t tx = 0;                                                \
        simple_buffer_take_from_isr(&(I2C_CFG.txRing), (uint8_t*)&tx);          \
        TINY_REGW(I2C_CFG.base + UCxnTXBUF) = tx;                               \
      }                                                                         \
    } break;                                                                    \
    case USCI_I2C_UCBCNTIFG:                                                    \
      break;                                                                    \
    case USCI_I2C_UCCLTOIFG:                                                    \
      /* Vector 24: Clock low timeout */                                        \
      TINY_REGW(I2C_CFG.base + UCBnIFG)   &= ~UCCLTOIFG;                        \
      scheduler_add_from_isr(i2c_isr_reinit, false);                            \
      break;                                                                    \
    default:                                                                    \
      break;                                                                    \
  }                                                                             \
}

uint8_t i2c_busy(struct i2c_cfg_t* i2c);
void i2c_init(struct i2c_cfg_t* i2c);
void i2c_deinit(struct i2c_cfg_t* i2c);
uint8_t i2c_send(struct i2c_cfg_t* i2c, uint8_t adr, uint8_t *buff, uint16_t len);

void i2c_set_irq_inst(struct i2c_cfg_t* i2c);
void i2c_isr_reinit();
void i2c_isr_tx_done();
void i2c_isr_rx_done();

#endif
