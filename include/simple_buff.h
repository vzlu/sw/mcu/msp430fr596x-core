#ifndef _MSP430FR596X_CIRC_BUFF_H
#define _MSP430FR596X_CIRC_BUFF_H

#include <stdint.h>

typedef struct
{
  int writePointer;
  int readPointer;
  int buffSize;
  unsigned char *buff;
} simple_buffer_t;


/* NON-thread safe functions
 *  - call from interrupt program (as in ISR the other interrupts are disable anyway-
 *  - use in case they thread safe is not necessary (e.g. called only from normal program, no threads
 *  - the function add/take check if buffer is empty/full
*/

void simple_buffer_init_from_isr    (volatile simple_buffer_t* sBuf, unsigned char* buffer, int bufferSize);
int simple_buffer_is_empty_from_isr (volatile simple_buffer_t* sBuf);
int simple_buffer_is_full_from_isr  (volatile simple_buffer_t* sBuf);
int simple_buffer_available_from_isr(volatile simple_buffer_t* sBuf);
int simple_buffer_take_from_isr     (volatile simple_buffer_t* sBuf, unsigned char *out);
int simple_buffer_add_from_isr      (volatile simple_buffer_t* sBuf, unsigned char ch);

/* Thread safe functions
 * - they disable the interrupts, call from normal program 
   - the function add/take check if buffer is empty/full
 */

void simple_buffer_init     (volatile simple_buffer_t* sBuf, unsigned char* buffer, int bufferSize);
int simple_buffer_is_empty  (volatile simple_buffer_t* sBuf);
int simple_buffer_is_full   (volatile simple_buffer_t* sBuf);
int simple_buffer_available (volatile simple_buffer_t* sBuf);
int simple_buffer_take      (volatile simple_buffer_t* sBuf, unsigned char *out);
int simple_buffer_add       (volatile simple_buffer_t* sBuf, unsigned char ch);



#endif
