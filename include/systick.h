#ifndef _MSP430FR596X_SYSTICK_H
#define _MSP430FR596X_SYSTICK_H

#include "core.h"

typedef void (*systick_handler_fce_t)(void);

void systick_init(systick_handler_fce_t cb, uint16_t div);
void systick_start();
void systick_stop();
uint32_t systick_uptime(void);
uint32_t systick_uptime_ms(void);

#endif // SYSTICK_H
