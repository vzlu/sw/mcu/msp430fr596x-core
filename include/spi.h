#ifndef _MSP430FR596X_SPI_H
#define _MSP430FR596X_SPI_H

#include "core.h"
#include "gpio.h"

struct spi_cfg_t {
  TINY_WORD_TYPE* periphBase;
  uint32_t freq;
  struct pin_t csPin;
  uint8_t initialized;
};

void spi_init(struct spi_cfg_t cfg);
void spi_blocking_transaction(struct spi_cfg_t cfg, uint8_t* tx_data, uint8_t* rx_data, uint8_t len);

#endif
