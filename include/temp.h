#ifndef _MSP430FR596X_TEMP_H
#define _MSP430FR596X_TEMP_H

#include "core.h"

void temp_init();

// get internal temperature in 0.1 deg. C
int16_t temp_blocking_get();

#endif
