#ifndef _MSP430FR596X_SCHEDULER_H
#define _MSP430FR596X_SCHEDULER_H

#include <stdbool.h>

#define SCHEDULER_EVENT_SIZE    (20)

typedef void (*scheduler_fce_t)(void);

/* NON-thread safe functions
 *  - call from interrupt program (as in ISR the other interrupts are disable anyway-
 *  - use in case they thread safe is not necessary (e.g. called only from normal program, no threads
 *  - the function add/take check if buffer is empty/full
*/

void scheduler_init_from_isr(void);
//If force = true the function will be added (if queue not full)
//if force = false AND the same function is already planned it will be not added again
void scheduler_add_from_isr(scheduler_fce_t fce, bool force);
void scheduler_take_from_isr(scheduler_fce_t *fce);

/* Thread safe functions
 * - they disable the interrupts, call from normal program 
   - the function add/take check if buffer is empty/full
 */

void scheduler_init(void);
//If force = true the function will be added (if queue not full)
//if force = false AND the same function is already planned it will be not added again
void scheduler_add(scheduler_fce_t fce, bool force);
void scheduler_take(scheduler_fce_t *fce);

#endif
