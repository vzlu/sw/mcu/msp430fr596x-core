#include "gpio.h"

void gpio_select(struct pin_t pin, uint8_t func) {
  switch(func) {
    case(GPIO_FUNC_DIGITAL_IO): {
      TINY_REGB(pin.port + PxSEL1) &= ~(1 << pin.bit);
      TINY_REGB(pin.port + PxSEL0) &= ~(1 << pin.bit);
    } break;
    
    case(GPIO_FUNC_PRIMARY): {
      TINY_REGB(pin.port + PxSEL1) &= ~(1 << pin.bit);
      TINY_REGB(pin.port + PxSEL0) |=  (1 << pin.bit);
    } break;
    
    case(GPIO_FUNC_SECONDARY): {
      TINY_REGB(pin.port + PxSEL1) |=  (1 << pin.bit);
      TINY_REGB(pin.port + PxSEL0) &= ~(1 << pin.bit);
    } break;
    
    case(GPIO_FUNC_TERTIARY): {
      TINY_REGB(pin.port + PxSEL1) |=  (1 << pin.bit);
      TINY_REGB(pin.port + PxSEL0) |=  (1 << pin.bit);
    } break;
  
  }
}

void gpio_set_output(struct pin_t pin) {
  gpio_select(pin, GPIO_FUNC_DIGITAL_IO);
  TINY_REGB(pin.port + PxDIR) |= (1 << pin.bit);
}

void gpio_set_input(struct pin_t pin) {
  gpio_select(pin, GPIO_FUNC_DIGITAL_IO);
  TINY_REGB(pin.port + PxDIR) &= ~(1 << pin.bit);
}

void gpio_set_analog(struct pin_t pin) {
  gpio_select(pin, GPIO_FUNC_TERTIARY);
  TINY_REGB(pin.port + PxDIR) &= ~(1 << pin.bit);
}

void gpio_set_level(struct pin_t pin, uint8_t lvl) {
  if(lvl) {
    TINY_REGB(pin.port + PxOUT) |= (1 << pin.bit);
  } else {
    TINY_REGB(pin.port + PxOUT) &= ~(1 << pin.bit);
  }
}

void gpio_toggle(struct pin_t pin) {
  TINY_REGB(pin.port + PxOUT) ^= (1 << pin.bit);
}

uint8_t gpio_read_port(struct pin_t pin) {
  return(TINY_REGB(pin.port + PxIN));
}

uint8_t gpio_read_pin(struct pin_t pin) {
  return((gpio_read_port(pin) >> pin.bit) & 0x01);
}
