#include "simple_buff.h"
#include "core.h"

void simple_buffer_init(volatile simple_buffer_t* sBuf, unsigned char* buffer, int bufferSize)
{
  _disable_interrupts();
  simple_buffer_init_from_isr(sBuf, buffer, bufferSize);
   _enable_interrupts();

}

int simple_buffer_is_empty(volatile simple_buffer_t* sBuf)
{
  int ret;

  _disable_interrupts();
  ret = simple_buffer_is_empty_from_isr(sBuf);
  _enable_interrupts();

  return ret;
}

int simple_buffer_is_full(volatile simple_buffer_t* sBuf)
{
  int ret;
  
  _disable_interrupts();
  ret = simple_buffer_is_full_from_isr(sBuf);
  _enable_interrupts();

  return ret;
}

int simple_buffer_available(volatile simple_buffer_t* sBuf)
{
  int ret;
  
  _disable_interrupts();
  ret = simple_buffer_available_from_isr(sBuf);
  _enable_interrupts();

  return ret;
}

int simple_buffer_take(volatile simple_buffer_t* sBuf, unsigned char *out)
{
  int ret;
  
  _disable_interrupts();
  ret = simple_buffer_take_from_isr(sBuf, out);
  _enable_interrupts();

  return ret;
}

int simple_buffer_add(volatile simple_buffer_t* sBuf, unsigned char ch)
{
  int ret;
  
  _disable_interrupts();
  ret = simple_buffer_add_from_isr(sBuf, ch);
  _enable_interrupts();

  return ret;
}



void simple_buffer_init_from_isr(volatile simple_buffer_t* sBuf, unsigned char *buffer, int bufferSize)
{
  if (!sBuf || !buffer || bufferSize < 1)
    return;
  sBuf->buff = buffer;
  sBuf->buffSize = bufferSize;
  sBuf->writePointer = 0;
  sBuf->readPointer = 0;
}

int simple_buffer_is_empty_from_isr(volatile simple_buffer_t* sBuf)
{
  if (!sBuf)
    return 1;

  int retVal;

  retVal = (sBuf->readPointer == sBuf->writePointer);

  return retVal;
}

int simple_buffer_is_full_from_isr(volatile simple_buffer_t* sBuf)
{
  if (!sBuf)
    return 0;

  int retVal;

  retVal = (((sBuf->writePointer + 1) % sBuf->buffSize) == sBuf->readPointer);

  return retVal;
}

int simple_buffer_available_from_isr(volatile simple_buffer_t* sBuf)
{
  if (!sBuf)
    return 0;
  int retVal;

  retVal = (sBuf->writePointer + sBuf->buffSize - sBuf->readPointer) % sBuf->buffSize;

  return retVal;
}

int simple_buffer_take_from_isr(volatile simple_buffer_t* sBuf, unsigned char *out)
{
  if (simple_buffer_is_empty_from_isr(sBuf))
    return 0;

  if (out)
    *out = sBuf->buff[sBuf->readPointer];

  sBuf->readPointer = (sBuf->readPointer + 1) % sBuf->buffSize;

  return 1;
}

int simple_buffer_add_from_isr(volatile simple_buffer_t* sBuf, unsigned char ch)
{
  int isFull = simple_buffer_is_full_from_isr(sBuf);

  if(!isFull)
  {
    sBuf->buff[sBuf->writePointer] = ch;
    sBuf->writePointer = (sBuf->writePointer + 1) % sBuf->buffSize;
  }

  return isFull;
}
