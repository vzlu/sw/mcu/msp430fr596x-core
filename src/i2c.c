#include "i2c.h"

#include "scheduler.h"
#include "gpio.h"

#define BUSY_TIMEOUT_TICKS(FREQ)    (100*core_get_smclk_freq()/FREQ)




// I2C instance that caused IRQ and is being handled
volatile static struct i2c_cfg_t* i2c_irq_inst = NULL;

void i2c_set_irq_inst(struct i2c_cfg_t* i2c) {
  i2c_irq_inst = i2c;
}

uint8_t i2c_busy(struct i2c_cfg_t* i2c) {
  return((uint8_t)(i2c->busy));
}

static void i2c_reinit(struct i2c_cfg_t* i2c) {
  // put peripheral to reset
  TINY_REGW(i2c->base + UCxnCTLW0) = UCSWRST;
  
  // I2C mode, use SMCLK as the clock source, synchronous mode, multi-master enabled
  TINY_REGW(i2c->base + UCxnCTLW0) |= (UCMODE_3 | UCSSEL__SMCLK | UCSYNC | UCMM);
  
  // set clock low timeout to 150000 MODCLK cycles
  TINY_REGW(i2c->base + UCxnCTLW1) = UCCLTO_2;
  
  // set I2C clock frequency
  TINY_REGW(i2c->base + UCxnBRW) = core_get_smclk_freq()/i2c->freq;
  
  // set slave address
  TINY_REGW(i2c->base + UCBnI2COA0) = i2c->addr | UCOAEN;
  
  // release from reset
  TINY_REGW(i2c->base + UCxnCTLW0) &= ~UCSWRST;
  
  // enable interrupts
  TINY_REGW(i2c->base + UCBnIE) = (UCRXIE0 | UCSTPIE | UCSTTIE | UCALIE | UCCLTOIE);
  
  // set internal variables
  i2c->busy = 0;
  i2c->rx = 1;
}

void i2c_init(struct i2c_cfg_t* i2c) {
  if(i2c->initialized) {
    return;
  }
  
  if(i2c->freq <= 0) {
    return;
  }

  // look up pins
  struct pin_t sda, scl;
  if(i2c->base == UCB0) {
    sda.port = P1;
    sda.bit = 6;
    scl.port = P1;
    scl.bit = 7;
  } else {
    // invalid base address
    return;
  }
  
  // set pin functions
  gpio_select(sda, GPIO_FUNC_SECONDARY);
  gpio_select(scl, GPIO_FUNC_SECONDARY);
  
  // low-level init
  i2c_reinit(i2c);
  
  // initialize internal variables
  simple_buffer_init(&(i2c->rxRing), i2c->rxBuffPtr, i2c->rxBuffSize);
  simple_buffer_init(&(i2c->txRing), i2c->txBuffPtr, i2c->txBuffSize);
  i2c->initialized = 1;
}

void i2c_deinit(struct i2c_cfg_t* i2c) {
  // configure GPIO
  if(i2c->base == UCB0) {
    P2SEL1 &= ~(BIT6 | BIT7);
    P2SEL0 &= ~(BIT6 | BIT7);
  } else {
    // invalid base address
    return;
  }
  
  // disable interrupts
  TINY_REGW(i2c->base + UCBnIE) &= ~(UCRXIE0 | UCSTPIE | UCSTTIE | UCALIE | UCCLTOIE);
  i2c->initialized = 0;
}

uint8_t i2c_send(struct i2c_cfg_t* i2c, uint8_t adr, uint8_t *buff, uint16_t len) {
  if(i2c_busy(i2c) || len > I2C_BUFF_SIZE) {
    return(0);
  }

  //Realy need this?
  int timeout = 0;
  while(TINY_REGW(i2c->base + UCBnSTATW) & UCBBUSY) {
    if(timeout++ >= BUSY_TIMEOUT_TICKS(i2c->freq)) {
      i2c_reinit(i2c);
      return(0);
    }
  }
  timeout = 0;
  while(TINY_REGW(i2c->base + UCBnSTATW) & UCBBUSY) {
    if(timeout++ >= BUSY_TIMEOUT_TICKS(i2c->freq)) {
      i2c_reinit(i2c);
      return(0);
    }
  }

  i2c->busy = 1;
  for(uint16_t i = 0; i < len; i++) {
    simple_buffer_add(&(i2c->txRing), buff[i]);
  }
    
  // enable master mode
  TINY_REGW(i2c->base + UCxnCTLW0) |=  UCSWRST;
  TINY_REGW(i2c->base + UCxnCTLW0) |=  (UCMM | UCMST);
  TINY_REGW(i2c->base + UCxnCTLW0) &= ~UCSWRST;
  
  // set destination address
  TINY_REGW(i2c->base + UCBnI2CSA) = adr;
  
  // clear any pending interrupts
  TINY_REGW(i2c->base + UCBnIFG) &= ~(UCTXIFG | UCRXIFG);
  
  // disable start and Rx interrupt, enable Tx and NACK interrupt
  TINY_REGW(i2c->base + UCBnIE) &= ~(UCSTTIE | UCRXIE);
  TINY_REGW(i2c->base + UCBnIE) |=  (UCTXIE0 | UCNACKIE);
  
  // start Tx
  TINY_REGW(i2c->base + UCxnCTLW0) |= (UCTR | UCTXSTT);

  return(1);
}

// IRQ service callbacks - these get called by scheduler, hence the need to pass the instance via global
void i2c_isr_reinit() {
  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)i2c_irq_inst;
  i2c_reinit(i2c);
}

void i2c_isr_tx_done() {
  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)i2c_irq_inst;
  uint16_t timeout = 0;
  
  while(TINY_REGW(i2c->base + UCBnSTATW) & UCBBUSY) {
    if(timeout++ >= BUSY_TIMEOUT_TICKS(i2c->freq)) {
      i2c_reinit(i2c);
      return;
    }
  }
  
  // disable master mode, return to slave
  TINY_REGW(i2c->base + UCxnCTLW0) |=  UCSWRST;
  TINY_REGW(i2c->base + UCxnCTLW0) &= ~UCMST;
  TINY_REGW(i2c->base + UCxnCTLW0) &= ~UCSWRST;
  
  // disable TX interrupt
  TINY_REGW(i2c->base + UCBnIE) &= ~UCTXIE;
  
  // enable START/STOP/RX interrupt
  TINY_REGW(i2c->base + UCBnIE) |= (UCSTTIE | UCRXIE | UCSTPIE);
  
  i2c->busy = 0;
  i2c->rx = 1;
}

void i2c_isr_rx_done() {
  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)i2c_irq_inst;
  i2c->busy = 0;
  
  if(i2c->rxCb) 
  {
    uint16_t len = simple_buffer_available(&(i2c->rxRing));


    i2c->rxCb(&(i2c->rxRing), len);

 }
}
