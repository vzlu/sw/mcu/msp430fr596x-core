#include "scheduler.h"
#include "core.h"

typedef struct
{
  volatile int writePointer;
  volatile int readPointer;
  scheduler_fce_t buff[SCHEDULER_EVENT_SIZE];
} scheduler_queue_t;

volatile scheduler_queue_t sInst;


void scheduler_init(void)
{
  _disable_interrupts();
  scheduler_init_from_isr();
  _enable_interrupts();
}

void scheduler_add(scheduler_fce_t fce, bool force)
{
  _disable_interrupts();
  scheduler_add_from_isr(fce, force);
  _enable_interrupts();
}

void scheduler_take(scheduler_fce_t *fce)
{
  _disable_interrupts();
  scheduler_take_from_isr(fce);
  _enable_interrupts();
}

void scheduler_init_from_isr()
{  
  sInst.writePointer = 0;
  sInst.readPointer = 0;
}

void scheduler_add_from_isr(scheduler_fce_t fce, bool force)
{
  if (((sInst.writePointer + 1) % SCHEDULER_EVENT_SIZE) == sInst.readPointer)
  {
    return;
  }

  uint8_t found;
  if (force == 1)
  {
    found = 0; //if force pretend we not found so we insert
  } 
  else if (force == 0)
  {
    found = 0;
    int i = sInst.readPointer;
    for (int j = 0; j < SCHEDULER_EVENT_SIZE; j++) //or while 1
    {
      if (i == sInst.writePointer)
      {
        break;
      }

      if (sInst.buff[i] == fce)
      {
        found = 1;
        break;
      }

      i = (i + 1) % SCHEDULER_EVENT_SIZE;
    }
  }

  if (found == 0)
  {
    sInst.buff[sInst.writePointer] = fce;
    sInst.writePointer = (sInst.writePointer + 1) % SCHEDULER_EVENT_SIZE;
  }

}

void scheduler_take_from_isr(scheduler_fce_t *fce)
{
  if (sInst.readPointer == sInst.writePointer)
  {
    *fce = 0;
    return;
  }

  if (fce)
  {
    *fce = sInst.buff[sInst.readPointer];
  }

  sInst.readPointer = (sInst.readPointer + 1) % SCHEDULER_EVENT_SIZE;

}
