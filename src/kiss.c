#include "kiss.h"

// TODO queue
static struct kiss_inst_t* inst_rx_done = NULL;

void kiss_rx_done() {

  if(inst_rx_done->cb) {
    // pass frame buffer
    inst_rx_done->cb(inst_rx_done->rxBuffPtr, inst_rx_done->rxBuffPos);

    // reset state machine
    inst_rx_done->mode = KISS_MODE_NOT_STARTED;
    inst_rx_done->rxBuffPos = 0;
  }
}

int kiss_send(struct kiss_inst_t* inst, uint8_t *buff, uint16_t len) {
  // special sequences
  const uint8_t start[] = { FEND, TNC_DATA };
  const uint8_t esc_end[] = { FESC, TFEND };
  const uint8_t esc_esc[] = { FESC, TFESC };
  const uint8_t stop[] = { FEND };

  // send start
  uart_write(inst->uart, (uint8_t*)start, sizeof(start));

  // send data
  uint8_t* data = buff;
  for(uint16_t i = 0; i < len; i++, ++data) {
    // escape FEND
    if(*data == FEND) {
      uart_write(inst->uart, (uint8_t*)esc_end, sizeof(esc_end));
      continue;
    }

    // escape FESC
    if(*data == FESC) {
      uart_write(inst->uart, (uint8_t*)esc_esc, sizeof(esc_esc));
      continue;
    }

    // send data byte
    uart_write(inst->uart, data, 1);
  }

  // send stop
  uart_write(inst->uart, (uint8_t*)stop, sizeof(stop));
  
  // make sure all transfers are finished in DMA mode
  if(inst->uart->txMode == DMA) {
    uart_finish_dma(inst->uart);
  }
  
  return(1);
}

void kiss_check(struct kiss_inst_t* inst, uint8_t b) {
  if(inst->rxBuffPos >= inst->rxBuffSize) {
    // overflow!
    return;
  }
  
  switch(inst->mode) {
    case KISS_MODE_NOT_STARTED: {
      // discard everything until FEND
      if(b != FEND) {
        return;
      }

      // got FEND, skip first byte (TNC_DATA)
      inst->mode = KISS_MODE_SKIP_TNC;
    } break;

    case KISS_MODE_SKIP_TNC: {
      // start transfer
      inst->mode = KISS_MODE_STARTED;
      inst->rxBuffPos = 0;
    } break;

    case KISS_MODE_STARTED: {
      // check escape byte
      if(b == FESC) {
        inst->mode = KISS_MODE_ESCAPED;
        return;
      }

      // check end byte
      if(b == FEND) {
        // got complete KISS packet, pass it to CSP
        inst_rx_done = inst;
        scheduler_add(kiss_rx_done, false);
        return;
      }

      // add to buffer
      inst->rxBuffPtr[inst->rxBuffPos++] = b;
    } break;

    case KISS_MODE_ESCAPED: {
      uint8_t escChar = b;

      // replace transposed characters
      if(b == TFESC) {
        // escaped escape char
        escChar = FESC;
      } else if(b == TFEND) {
        // escaped end char
        escChar = FEND;
      }

      // add to buffer
      inst->rxBuffPtr[inst->rxBuffPos++] = escChar;
      inst->mode = KISS_MODE_STARTED;
    } break;
  }

}
